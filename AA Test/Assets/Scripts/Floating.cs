using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    public float rotSpeed;
    public float floatSpeed;
    public float floatSpread;

    float floatTimer = 0;
    float timer = 0;
    Vector3 initialRot;
    Vector3 initialPos;

    private void Start()
    {
        initialPos = transform.position;
    }

    void Update()
    {
        initialRot = transform.eulerAngles;
        timer += Time.deltaTime * rotSpeed;
        floatTimer += Time.deltaTime * floatSpeed;
        Float();
    }

    void Float()
    {
        float y = timer;

        transform.eulerAngles = new Vector3(initialRot.x, y, initialRot.z);

        float yPos = Mathf.Sin(floatTimer) * floatSpread;
        Vector3 floatPos = new Vector3(0, yPos, 0);

        transform.position = floatPos + initialPos;
    }
}
