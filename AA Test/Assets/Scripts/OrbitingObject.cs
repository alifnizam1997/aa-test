using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitingObject : MonoBehaviour
{
	public float xSpread;
	public float zSpread;
	public float ySpread;
	public Transform centerPoint;

	public float rotSpeed;
	public bool rotateClockwise;

	float timer = 0;

	// Update is called once per frame
	void Update()
	{
		timer += Time.deltaTime * rotSpeed;
		Rotate();
	}

	void Rotate()
	{
		if (rotateClockwise)
		{
			float x = -Mathf.Cos(timer) * xSpread;
			float z = Mathf.Sin(timer) * zSpread;
			float y = Mathf.Sin(timer) * ySpread;
			Vector3 pos = new Vector3(x, y, z);
			transform.position = pos + centerPoint.position;
		}
		else
		{
			float x = Mathf.Cos(timer) * xSpread;
			float z = Mathf.Sin(timer) * zSpread;
			float y = Mathf.Sin(timer) * ySpread;
			Vector3 pos = new Vector3(x, y, z);
			transform.position = pos + centerPoint.position;
		}
	}
}
